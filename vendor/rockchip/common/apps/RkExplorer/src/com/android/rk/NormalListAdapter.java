package com.android.rk;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnGenericMotionListener;

import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import android.os.Environment;
import android.util.Log;

/**
 * GridView adapter to show the list of applications and shortcuts  GalleryAdapter
 */
/*	*/
public class NormalListAdapter extends ArrayAdapter<FileInfo>{
	private final String TAG = "NormalListAdapter";
	private final LayoutInflater mInflater;
	private ArrayList<FileInfo> textview;
	
	private Resources resources;
	public NormalListAdapter(Context context, ArrayList<FileInfo> files) {
		super(context, 0, files);
		mInflater = LayoutInflater.from(context);
		textview = files;
		resources = context.getResources();
	}
    @Override  
    public int getCount() {  
        return textview == null? 0 :textview.size();  
    } 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		FileInfo info = getItem(position);
		ViewHolder holder = null;  
		if (convertView == null) {
		    holder = new ViewHolder();  
			//convertView = mInflater.inflate(R.layout.folder_listview_adapter, parent, false);
			convertView = mInflater.inflate(R.layout.normal_adapter, null);
			holder.image = (ImageView) convertView.findViewById(R.id.nor_image);
    		holder.text = (TextView) convertView.findViewById(R.id.nor_text);
    		holder.text_right = (TextView) convertView.findViewById(R.id.nor_right_text);
    		convertView.setTag(holder);
		} else {
            holder = (ViewHolder) convertView.getTag(); 
		}
		//-----------------------------
        
        holder.image.setImageDrawable(info.icon);
        boolean hasFileName = info.fileName != null && !info.fileName.trim().equals("");
        holder.text_right.setText("");
        if(info.isDir){
            if(ConfigUtil.USE_STORAGEMANAGER_TO_FILL && hasFileName)
            	holder.text.setText(info.fileName);
            else if(info.file.getPath().equals(RockExplorer.flash_dir))
            	holder.text.setText(resources.getString(R.string.str_flash_name));
            else if(info.file.getPath().equals(RockExplorer.sdcard_dir))
            	holder.text.setText(resources.getString(R.string.str_sdcard_name));
            else if(info.file.getPath().equals(RockExplorer.internal_dir))
            	holder.text.setText(resources.getString(R.string.str_internal_name));
            else if(RockExplorer.isInList(RockExplorer.usb_dir_list, info.file.getPath()) == true){
                String id = "";
                if(RockExplorer.usb_dir_list.size() > 1){
                    id = "(" + info.file.getPath().substring(info.file.getPath().lastIndexOf('/')+1) + ")";
                }
            	if(ConfigUtil.NOT_USE_SDCRAD){
            		holder.text.setText(resources.getString(R.string.str_usb2_name) + id);
            	}else{
            		holder.text.setText(resources.getString(R.string.str_usb1_name) + id);
            	}
            }else{
                holder.text_right.setText(getFileAttribute(info));
                holder.text.setText(info.file.getName());
            }
        }else{
            holder.text_right.setText(getFileAttribute(info));
        	holder.text.setText(info.file.getName());
        }

        if(!info.isChoice){
            holder.text.setTextColor(holder.text.getResources().getColor(R.color.text));
				 }else{
            holder.text.setTextColor(holder.text.getResources().getColor(R.color.text_choice));
				 }
            
        convertView.setOnGenericMotionListener(ogml);
		return convertView;
	}	
	
	/*
	 * 如下为将一个文件大小的Long型数据转换成K/M/G类型来表示，且只保留小数点后两位~
	 * */
	public String change_Long_to_String(long temp_change){
		String temp_str = new String("");
		DecimalFormat df = new DecimalFormat("########.00");	//取float的小数点后两位   
		//四舍五入   
		
		if(temp_change >= 1024){	//计算 K
			float i =  temp_change / 1024f;
			if(i >= 1024f){			//计算M
				float j = i / 1024f;
				if(j >= 1024f){		//计算G
					float k = j / 1024f;
                                        temp_str += df.format(k);
                                        temp_str += " G";
				}else{
					temp_str += df.format(j);
					temp_str += " M";
				}
			}else{
				temp_str += df.format(i);
				temp_str += " K";
			}
		}else{
			temp_str += temp_change;
			temp_str += " B";
		}
		return temp_str;
	}
	
	/*
	 * 如下为将一long型时间转换成时间方式表示
	 * */
	private String change_long_to_time(long temp_time){
		//java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("M/dd/yyyy hh:mm:ss a",java.util.Locale.US); 
		//java.util.Date d = sdf.parse("5/13/2003 10:31:37 AM");  
		//long dvalue=d.getTime();  
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		String mDateTime1=formatter.format(temp_time); 
		return mDateTime1;
	}
	
	/*
	 * 如下为获取一个文件的读写信息，是否是目录、可读、可写 
	 * */
	private String getAttribute(File file){
		String temp_Attr = new String("");
		if(file.isDirectory()){
			temp_Attr += "d";
		}else{
			temp_Attr +="-";
		}
		
		if(file.canRead()){
			temp_Attr += "r";
		}else{
			temp_Attr +="-";
		}
		
		if(file.canWrite()){
			temp_Attr += "w";
		}else{
			temp_Attr +="-";
		}
		
		return temp_Attr;
	}
	private String getFileAttribute(FileInfo info){
        String temp_right = null;
        if(info.isDir){
            temp_right = new String("");
        }else{
            long temp_size = info.file.length();
            temp_right = new String("");
            temp_right += change_Long_to_String(temp_size);
        }
        temp_right += " | ";
        long lastModified = info.file.lastModified();
        temp_right += change_long_to_time(lastModified);
        temp_right += " | ";
        temp_right += getAttribute(info.file);
        info.isWrite = info.file.canWrite();
        info.isRead = info.file.canRead();
        return temp_right;
    }
	
	
	
	OnGenericMotionListener ogml = new OnGenericMotionListener () {

		@Override
		public boolean onGenericMotion(View v, MotionEvent event) {
			// TODO Auto-generated method stub
			int what = event.getButtonState();
			switch (what) {
			/*case MotionEvent.ACTION_DOWN: // 鼠标悬浮在上方
				System.out.println("ACTION_DOWN");
				break;	
			case MotionEvent.BUTTON_PRIMARY: // 鼠标左键
				System.out.println("BUTTON_PRIMARY");
				break;	
			case MotionEvent.BUTTON_TERTIARY: // 鼠标中键
				System.out.println("BUTTON_TERTIARY");
				break;*/		
			case MotionEvent.BUTTON_SECONDARY: // 鼠标右键
				RockExplorer.isMouseRightClick = true;
				break;	
			}
			return false;
		}
		
	};
	class ViewHolder{
        ImageView image;
    	TextView text;
    	TextView text_right;
    }
}
