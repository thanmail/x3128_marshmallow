LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= util.c asm-opt.c arm-neon.S main.c x86-sse2.S mips-32.S
LOCAL_MODULE := tinymembench
LOCAL_MODULE_TAGS := debug eng tests
include $(BUILD_EXECUTABLE)
